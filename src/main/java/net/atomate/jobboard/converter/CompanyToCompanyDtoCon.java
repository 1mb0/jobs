package net.atomate.jobboard.converter;

import net.atomate.jobboard.dto.CompanyDto;
import net.atomate.jobboard.model.Company;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class CompanyToCompanyDtoCon implements Converter<Company, CompanyDto>
{
    @Override
    public CompanyDto convert(final Company company)
    {
        return CompanyDto.builder()
                .companyName(company.getCompanyName())
                .contactNumber(company.getContactNumber())
                .contactPerson(company.getContactPerson())
                .companyDescription(company.getCompanyDescription())
                .webPage(company.getWebPage())
                .email(company.getEmail())
                .build();
    }
}
