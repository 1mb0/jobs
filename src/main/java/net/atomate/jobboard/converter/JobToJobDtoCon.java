package net.atomate.jobboard.converter;

import net.atomate.jobboard.dto.JobDto;
import net.atomate.jobboard.model.Job;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class JobToJobDtoCon implements Converter<Job, JobDto>
{
    @Override
    public JobDto convert(final Job job)
    {
        return JobDto.builder()
                .jobName(job.getJobName())
                .category(job.getCategory())
                .description(job.getDescription())
                .company(job.getCompany())
                .salary(job.getSalary())
                .city(job.getCity())
                .build();
    }
}
