package net.atomate.jobboard.web;

import com.querydsl.core.types.Predicate;
import lombok.RequiredArgsConstructor;
import net.atomate.jobboard.model.Job;
import net.atomate.jobboard.repository.CategoryRepository;
import net.atomate.jobboard.service.CompanyService;
import net.atomate.jobboard.service.JobService;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;


@Controller
@RequiredArgsConstructor
public class WebController
{
    private final CompanyService companyService;

    private final JobService jobService;

    private final CategoryRepository categoryRepository;


    @GetMapping("jobs/by-company")
    public ModelAndView jobsByCompany(@PageableDefault(size = 12) final Pageable pageable)
    {
        final ModelAndView modelAndView = new ModelAndView();

        modelAndView.addObject("companies", companyService.findAll(pageable));

        modelAndView.setViewName("companies");

        return modelAndView;
    }

    @GetMapping("jobs/by-categories")
    public ModelAndView jobsByCategories()
    {
        final ModelAndView modelAndView = new ModelAndView();

        modelAndView.addObject("categories", categoryRepository.findAll());

        modelAndView.setViewName("jobsByCategories");

        return modelAndView;
    }

    @GetMapping("jobs/by-category/{id}")
    public ModelAndView jobsByCategory(@PathVariable final Integer id)
    {
        final ModelAndView modelAndView = new ModelAndView();

        modelAndView.addObject("jobs", jobService.findByCategory(id));

        modelAndView.setViewName("jobsByCategory");

        return modelAndView;
    }

    @GetMapping("/")
    public ModelAndView home(@QuerydslPredicate(root = Job.class) final Predicate predicate,
                             @PageableDefault(size = 20) final Pageable pageable,
                             final Principal principal)
    {
        final ModelAndView modelAndView = new ModelAndView();

        modelAndView.setViewName("home");

        if (principal != null)
        {
            modelAndView.addObject("company", companyService.findAllByUserId(Integer.parseInt(principal.getName())));
        }


        modelAndView.addObject("categories", categoryRepository.findAll());

        modelAndView.addObject("jobs", jobService.search(predicate, pageable));

        return modelAndView;
    }

}
