package net.atomate.jobboard.web;

import lombok.RequiredArgsConstructor;
import net.atomate.jobboard.dto.CompanyRegisterDto;
import net.atomate.jobboard.model.Application;
import net.atomate.jobboard.service.ApplicationService;
import net.atomate.jobboard.service.CompanyService;
import net.atomate.jobboard.service.JobService;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.security.Principal;

@Controller
@RequiredArgsConstructor
public class CompanyController
{
    private final CompanyService companyService;

    private final JobService jobService;

    private final ApplicationService applicationService;

    @GetMapping("/new-company")
    public ModelAndView register()
    {
        final ModelAndView modelAndView = new ModelAndView();

        modelAndView.setViewName("newCompany");

        modelAndView.getModelMap().get("msgAddCompany");

        modelAndView.addObject("companyDto", new CompanyRegisterDto());

        return modelAndView;
    }

    @PostMapping("/new-company")
    public ModelAndView registration(@Valid final CompanyRegisterDto companyDto, final BindingResult bindingResult,
                                     final Principal principal, final RedirectAttributes redirectAttributes)
    {
        final ModelAndView modelAndView = new ModelAndView();

        try
        {
            if (bindingResult.hasErrors())
            {
                modelAndView.addObject("companyDto", companyDto);
                modelAndView.setViewName("newCompany");
            }
            else
            {
                companyService.registerCompany(companyDto, principal);
                redirectAttributes.addFlashAttribute("msgAddCompany", "Company " + companyDto.getCompanyName() + " added successfully");
                modelAndView.setViewName("redirect:/my-company");
            }
        }
        catch (Exception exception)
        {
            modelAndView.addObject("companyDto", companyDto);
            modelAndView.setViewName("newCompany");
        }

        return modelAndView;
    }

    @GetMapping("/my-company")
    public ModelAndView showCompany(final Principal principal)
    {
        final ModelAndView modelAndView = new ModelAndView();

        modelAndView.setViewName("myCompanies");

        Integer userId = Integer.parseInt(principal.getName());

        modelAndView.addObject("userId", userId);

        modelAndView.addObject("companies", companyService.findAllByUserId(userId));

        return modelAndView;
    }

    @GetMapping("/downloadfile/{fileId}")
    public ResponseEntity<Resource> downloadFile(@PathVariable final Integer fileId) throws Exception
    {
        final Application application = applicationService.get(fileId);

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(application.getFileType()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + application.getFileName() + "\"")
                .body(new ByteArrayResource(application.getDataCv()));
    }

    @GetMapping("/applications/{id}")
    public ModelAndView applicationsByJob(final @PathVariable Integer id, final Pageable pageable) throws Exception
    {
        final ModelAndView modelAndView = new ModelAndView();

        modelAndView.addObject("applicants", applicationService.findByJobId(id, pageable));

        modelAndView.addObject("job", jobService.findById(id));

        modelAndView.setViewName("showApplicants");

        return modelAndView;
    }


}