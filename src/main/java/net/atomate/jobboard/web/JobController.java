package net.atomate.jobboard.web;

import lombok.RequiredArgsConstructor;
import net.atomate.jobboard.dto.ApplicationDto;
import net.atomate.jobboard.dto.JobDto;
import net.atomate.jobboard.service.ApplicationService;
import net.atomate.jobboard.service.CategoryService;
import net.atomate.jobboard.service.CompanyService;
import net.atomate.jobboard.service.JobService;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.security.Principal;

@Controller
@RequiredArgsConstructor
public class JobController
{
    private final CompanyService companyService;

    private final JobService jobService;

    private final ApplicationService applicationService;

    private final CategoryService categoryService;

    @GetMapping("/edit/{id}")
    public ModelAndView editForm(@PathVariable final Integer id) throws Exception
    {
        final ModelAndView modelAndView = new ModelAndView();

        modelAndView.setViewName("editJob");

        modelAndView.addObject("job", jobService.findById(id));

        return modelAndView;
    }

    @PostMapping("/edit/{id}")
    public ModelAndView updateJob(@Valid final JobDto jobDto, @PathVariable final Integer id,
                                  final BindingResult bindingResult, final RedirectAttributes redirectAttributes)
    {
        final ModelAndView modelAndView = new ModelAndView();

        try
        {
            if (bindingResult.hasErrors())
            {
                modelAndView.addObject("jobDto", jobDto);
                modelAndView.setViewName("editJob");
            }
            else
            {
                jobService.edit(jobDto, id);
                redirectAttributes.addFlashAttribute("msgUpdate", "Job " + jobService.findById(id)
                        .getJobName() + " was updated!");
                modelAndView.setViewName("redirect:/jobs/by-company/" + jobService.findById(id).getCompany().getId());
            }
        }
        catch (Exception exception)
        {
            modelAndView.addObject("jobDto", jobDto);
            modelAndView.setViewName("editJob");
        }

        return modelAndView;

    }

    @GetMapping("/jobs/create/{id}")
    public ModelAndView showCreateForm(@PathVariable final Integer id)
    {
        final ModelAndView modelAndView = new ModelAndView();

        modelAndView.setViewName("addJob");

        modelAndView.addObject("jobDto", new JobDto());

        modelAndView.addObject("categories", categoryService.findAll());

        return modelAndView;
    }

    @PostMapping("/jobs/create/{id}")
    public ModelAndView saveJobs(@Valid final JobDto jobDto, final BindingResult bindingResult,
                                 final Principal principal, final @PathVariable Integer id,
                                 final RedirectAttributes redirectAttributes)
    {
        final ModelAndView modelAndView = new ModelAndView();

        try
        {
            if (bindingResult.hasErrors())
            {
                modelAndView.addObject("jobDto", jobDto);
                modelAndView.setViewName("addJob");
            }
            else
            {
                jobService.save(jobDto, principal, id);
                System.out.println(jobDto);
                redirectAttributes.addFlashAttribute("msg", "Job added successfully!");
                modelAndView.setViewName("redirect:/jobs/by-company/" + id);
            }
        }
        catch (Exception exception)
        {
            modelAndView.addObject("jobDto", jobDto);
            modelAndView.setViewName("addJob");
        }

        return modelAndView;
    }

    @GetMapping("/jobs/{id}")
    public ModelAndView jobById(@PathVariable final Integer id) throws Exception
    {
        final ModelAndView modelAndView = new ModelAndView();

        modelAndView.addObject("job", jobService.findById(id));

        modelAndView.addObject("applicationDto", new ApplicationDto());

        modelAndView.addObject("company", jobService.findById(id).getCompany());

        modelAndView.setViewName("jobById");

        return modelAndView;
    }

    @PostMapping("/jobs/{id}")
    public ModelAndView addCV(@PathVariable final Integer id,
                              @Valid final ApplicationDto application,
                              final BindingResult bindingResult) throws Exception
    {
        final ModelAndView modelAndView = new ModelAndView();

        modelAndView.addObject("job", jobService.findById(id));

        try
        {
            if (bindingResult.hasErrors())
            {
                modelAndView.addObject("application", application);
                modelAndView.setViewName("jobById");
            }
            else
            {
                applicationService.save(application, id);
                modelAndView.setViewName("redirect:/jobs/" + id);
            }
        }
        catch (Exception exception)
        {
            modelAndView.addObject("application", application);
            modelAndView.setViewName("jobById");
        }

        return modelAndView;
    }

    @GetMapping("jobs/by-company/{id}")
    public ModelAndView showCompanyJobs(@PathVariable final Integer id) throws Exception
    {
        final ModelAndView modelAndView = new ModelAndView();

        modelAndView.addObject("jobs", jobService.findByCompanyId(id));

        modelAndView.getModelMap().get("msg");

        modelAndView.getModelMap().get("msgDelete");

        modelAndView.getModelMap().get("msgUpdate");

        modelAndView.addObject("company", companyService.findById(id));

        modelAndView.setViewName("manageCompany");

        return modelAndView;
    }

    @GetMapping("jobs/show-by-company/{id}")
    public ModelAndView showJobsOfCompany(@PathVariable final Integer id) throws Exception
    {
        final ModelAndView modelAndView = new ModelAndView();

        modelAndView.addObject("jobs", jobService.findByCompanyId(id));

        modelAndView.addObject("applicationDto", new ApplicationDto());

        modelAndView.addObject("company", companyService.findById(id));

        modelAndView.setViewName("jobsByCompany");

        return modelAndView;
    }

    @GetMapping("jobs/delete/{id}")
    public ModelAndView deleteProduct(@PathVariable final Integer id,
                                      final RedirectAttributes redirectAttributes) throws Exception
    {

        final ModelAndView modelAndView = new ModelAndView();

        redirectAttributes.addFlashAttribute("msgDelete", "Job " + jobService.findById(id)
                .getJobName() + " was deleted!");

        modelAndView.setViewName("redirect:/jobs/by-company/" + jobService.findById(id).getCompany().getId());

        jobService.delete(id);

        return modelAndView;
    }
}