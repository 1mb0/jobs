package net.atomate.jobboard.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import net.atomate.jobboard.model.Job;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@ToString
public class ApplicationDto
{
    @Size(min = 2, max = 40)
    @NotEmpty
    private String applicantName;

    @Email
    private String email;

    private Job job;

    private MultipartFile cv;
}
