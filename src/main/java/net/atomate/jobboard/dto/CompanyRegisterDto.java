package net.atomate.jobboard.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Lob;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@ToString
public class CompanyRegisterDto
{
    @NotEmpty
    @Size(min = 2, max = 40)
    private String companyName;

    @NotEmpty
    @Size(min = 2, max = 40)
    private String contactPerson;

    @NotEmpty
    private String contactNumber;

    private String webPage;

    @Email
    private String email;

    private Integer userId;

    @NotEmpty
    @Lob
    private String companyDescription;


}
