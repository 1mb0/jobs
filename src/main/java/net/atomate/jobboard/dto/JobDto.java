package net.atomate.jobboard.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import net.atomate.jobboard.model.Category;
import net.atomate.jobboard.model.Company;

import javax.persistence.Lob;
import javax.persistence.PrePersist;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@ToString
public class JobDto
{
    @Size(min = 2, max = 40)
    @NotEmpty
    private String jobName;

    @Lob
    @NotEmpty
    private String description;

    private Category category;

    private Company company;

    @NotNull
    private Double salary;

    @Size(min = 2, max = 40)
    @NotEmpty
    private String city;

}

