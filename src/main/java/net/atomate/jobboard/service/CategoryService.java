package net.atomate.jobboard.service;

import net.atomate.jobboard.model.Category;

import java.util.List;

public interface CategoryService
{
    List<Category> findAll();
}
