package net.atomate.jobboard.service;

import net.atomate.jobboard.dto.CompanyDto;
import net.atomate.jobboard.dto.CompanyRegisterDto;
import net.atomate.jobboard.model.Company;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.security.Principal;
import java.util.List;

public interface CompanyService
{
    Page<Company> findAll(Pageable pageable);

    List<Company> findByCompanyName(String companyName);

    Company save(Company company);

    CompanyDto registerCompany(CompanyRegisterDto userDto, Principal principal);

    List<Company> findAllByUserId(Integer id);

    Company findByUserId(Integer id);

    Company findById(Integer id) throws Exception;
}
