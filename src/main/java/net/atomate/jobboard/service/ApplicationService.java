package net.atomate.jobboard.service;

import net.atomate.jobboard.dto.ApplicationDto;
import net.atomate.jobboard.model.Application;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ApplicationService
{
    Application save(ApplicationDto file, Integer id) throws Exception;

    Application get(Integer id) throws Exception;

    Page<Application> findByJobId(Integer id, Pageable pageable);
}
