package net.atomate.jobboard.service.impl;

import lombok.RequiredArgsConstructor;
import net.atomate.jobboard.model.Category;
import net.atomate.jobboard.repository.CategoryRepository;
import net.atomate.jobboard.service.CategoryService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CategoryServiceImpl implements CategoryService
{
    private final CategoryRepository categoryRepository;

    @Override
    public List<Category> findAll()
    {
        return categoryRepository.findAll();
    }
}
