package net.atomate.jobboard.service.impl;

import com.querydsl.core.types.Predicate;
import lombok.RequiredArgsConstructor;
import net.atomate.jobboard.converter.JobToJobDtoCon;
import net.atomate.jobboard.dto.JobDto;
import net.atomate.jobboard.model.Job;
import net.atomate.jobboard.repository.CategoryRepository;
import net.atomate.jobboard.repository.CompanyRepository;
import net.atomate.jobboard.repository.JobRepository;
import net.atomate.jobboard.service.JobService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.List;

@Service
@RequiredArgsConstructor
public class JobServiceImpl implements JobService
{
    private final JobRepository jobRepository;

    private final CompanyRepository companyRepository;

    private final CategoryRepository categoryRepository;

    private final JobToJobDtoCon jobToJobDtoCon;

    @Override
    public List<Job> findByCategory(final Integer id)
    {
        return jobRepository.findByCategory_Id(id);
    }

    @Override
    public List<Job> findAll()
    {
        return jobRepository.findAll();
    }

    @Override
    public JobDto save(final JobDto jobDto, final Principal principal, final Integer id) throws Exception
    {
        final Job newJob = Job.builder()
                .jobName(jobDto.getJobName())
                .category(categoryRepository.findById(jobDto.getCategory().getId()).orElseThrow(()-> new Exception("No such category")))
                .description(jobDto.getDescription())
                .salary(jobDto.getSalary())
                .city(jobDto.getCity())
                .company(companyRepository.findById(id).orElseThrow(() -> new Exception("Company doesnt exist.")))
                .build();

        return jobToJobDtoCon.convert(jobRepository.save(newJob));
    }

    @Override
    public List<Job> findByCompanyId(final Integer id)
    {
        return jobRepository.findByCompanyId(id);
    }

    @Override
    public Job findById(final Integer id) throws Exception
    {
        return jobRepository.findById(id).orElseThrow(() -> new Exception("Doesn't exist"));
    }

    @Override
    public Page<Job> search(final Predicate predicate, final Pageable pageable)
    {
        return jobRepository.findAll(predicate, pageable);
    }

    @Override
    public void edit(final JobDto jobDto, final Integer id) throws Exception
    {
        final Job job = findById(id);

        editProduct(jobDto, job);
    }

    @Override
    public void delete(final Integer id)
    {
        jobRepository.deleteById(id);
    }

    private void editProduct(final JobDto jobDto, final Job job)
    {
        job.setJobName(jobDto.getJobName());
        job.setCity(jobDto.getCity());
        job.setSalary(jobDto.getSalary());
        job.setDescription(jobDto.getDescription());

        jobRepository.save(job);
    }

}
