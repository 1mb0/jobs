package net.atomate.jobboard.service.impl;

import lombok.RequiredArgsConstructor;
import net.atomate.jobboard.converter.CompanyToCompanyDtoCon;
import net.atomate.jobboard.dto.CompanyDto;
import net.atomate.jobboard.dto.CompanyRegisterDto;
import net.atomate.jobboard.model.Company;
import net.atomate.jobboard.repository.CompanyRepository;
import net.atomate.jobboard.service.CompanyService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CompanyServiceImpl implements CompanyService
{
    private final CompanyRepository companyRepository;

    private final CompanyToCompanyDtoCon companyDtoCon;

    @Override
    public Page<Company> findAll(final Pageable pageable)
    {
        return companyRepository.findAll(pageable);

    }

    @Override
    public List<Company> findByCompanyName(final String companyName)
    {
        return companyRepository.findByCompanyName(companyName);
    }

    @Override
    public Company save(final Company company)
    {
        return companyRepository.save(company);
    }

    @Override
    public CompanyDto registerCompany(final CompanyRegisterDto company, final Principal principal)
    {
        final Company newCompany = Company.builder()
                .companyName(company.getCompanyName())
                .contactNumber(company.getContactNumber())
                .contactPerson(company.getContactPerson())
                .companyDescription(company.getCompanyDescription())
                .webPage(company.getWebPage())
                .email(company.getEmail())
                .userId(Integer.parseInt(principal.getName()))
                .build();

        companyRepository.save(newCompany);

        return companyDtoCon.convert(newCompany);
    }

    @Override
    public List<Company> findAllByUserId(final Integer id)
    {
        return companyRepository.findAllByUserId(id);
    }

    @Override
    public Company findByUserId(final Integer id)
    {
        return companyRepository.findByUserId(id);
    }

    @Override
    public Company findById(final Integer id) throws Exception
    {
        return companyRepository.findById(id).orElseThrow(() -> new Exception("No such company"));
    }

}
