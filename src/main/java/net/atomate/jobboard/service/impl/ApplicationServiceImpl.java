package net.atomate.jobboard.service.impl;

import lombok.RequiredArgsConstructor;
import net.atomate.jobboard.dto.ApplicationDto;
import net.atomate.jobboard.model.Application;
import net.atomate.jobboard.model.Job;
import net.atomate.jobboard.repository.ApplicationRepository;
import net.atomate.jobboard.service.ApplicationService;
import net.atomate.jobboard.service.JobService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.Objects;
import java.util.Optional;


@Service
@RequiredArgsConstructor
public class ApplicationServiceImpl implements ApplicationService
{
    private final ApplicationRepository applicationRepository;

    private final JobService jobService;

    @Override
    public Application save(final ApplicationDto file, final Integer id) throws Exception
    {
        final Job job = Optional.ofNullable(jobService.findById(id)).orElseThrow(() -> new Exception("Asdsa"));
        final String fileName = StringUtils.cleanPath(Objects.requireNonNull(file.getCv().getOriginalFilename()));
        try
        {
            if (fileName.contains(".."))
            {
                throw new Exception("Sorry! Filename contains invalid path sequence " + fileName);
            }
            final Application build = Application.builder()
                    .applicantName(file.getApplicantName())
                    .email(file.getEmail())
                    .fileName(fileName)
                    .fileType(file.getCv().getContentType())
                    .dataCv(file.getCv().getBytes())
                    .job(job)
                    .build();

            return applicationRepository.save(build);
        }
        catch (IOException ex)
        {
            throw new Exception("Could not store file " + fileName + ". Please try again!", ex);
        }
    }

    @Override
    public Application get(final Integer id) throws Exception
    {
        return applicationRepository.findById(id)
                .orElseThrow(() -> new Exception("File not found with id " + id));
    }

    @Override
    public Page<Application> findByJobId(final Integer id, final Pageable pageable)
    {
        return applicationRepository.findByJob_Id(id, pageable);
    }

}
