package net.atomate.jobboard.service;

import com.querydsl.core.types.Predicate;
import net.atomate.jobboard.dto.JobDto;
import net.atomate.jobboard.model.Job;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.security.Principal;
import java.util.List;

public interface JobService
{
    List<Job> findByCategory(Integer id);

    List<Job> findAll();

    JobDto save(JobDto jobDto, Principal principal, Integer id) throws Exception;

    List<Job> findByCompanyId(Integer id);

    Job findById(Integer id) throws Exception;

    Page<Job> search(Predicate predicate, Pageable pageable);

    void edit(JobDto jobDto, Integer id) throws Exception;

    void delete(Integer id);

}
