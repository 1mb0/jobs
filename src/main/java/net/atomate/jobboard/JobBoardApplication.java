package net.atomate.jobboard;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.web.config.EnableSpringDataWebSupport;

@SpringBootApplication
@EnableSpringDataWebSupport
public class JobBoardApplication
{

    public static void main(final String[] args)
    {
        SpringApplication.run(JobBoardApplication.class, args);
    }

}
