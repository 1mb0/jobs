package net.atomate.jobboard.repository;

import net.atomate.jobboard.model.Company;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface CompanyRepository extends JpaRepository<Company, Integer>, PagingAndSortingRepository<Company, Integer>
{
    List<Company> findByCompanyName(String companyName);

    List<Company> findAllByUserId(Integer id);

    Company findByUserId(Integer id);
}
