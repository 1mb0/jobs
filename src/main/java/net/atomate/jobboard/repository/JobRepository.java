package net.atomate.jobboard.repository;

import net.atomate.jobboard.model.Job;
import net.atomate.jobboard.model.QJob;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public interface JobRepository extends JpaRepository<Job, Integer>, QuerydslPredicateExecutor<Job>, QuerydslBinderCustomizer<QJob>
{
    List<Job> findByCategory_Id(Integer id);

    List<Job> findByCompanyId(Integer id);

    @Override
    default void customize(@NotNull QuerydslBindings bindings, @NotNull QJob job)
    {
        bindings.bind(job.jobName).first((path, value) -> path.toLowerCase().contains(value.toLowerCase())); // 1

        bindings.bind(job.salary).first((path, value) -> path.between(value, value + 3000));

        bindings.bind(job.category)
                .all((path, collection) -> Optional.ofNullable(path.in(collection.stream()
                        .collect(Collectors.toList()))));

        bindings.bind(job.city).first((path, value) -> path.toLowerCase().eq(value.toLowerCase()));
    }
}

