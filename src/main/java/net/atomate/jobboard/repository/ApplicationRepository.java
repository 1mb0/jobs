package net.atomate.jobboard.repository;

import net.atomate.jobboard.model.Application;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ApplicationRepository extends JpaRepository<Application, Integer>, PagingAndSortingRepository<Application, Integer>
{
    Page<Application> findByJob_Id(Integer id, Pageable pageable);
}